"""Create User table

Revision ID: b7f9d25fecff
Revises: 
Create Date: 2019-04-12 22:25:08.300789

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b7f9d25fecff'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
		'users',
		sa.Column('id', sa.String(36), primary_key=True),
		sa.Column('first_name', sa.String(255)),
		sa.Column('last_name', sa.String(255)),
		sa.Column('email', sa.String(255), unique=True),
		sa.Column('uphold_id', sa.String(255), unique=True),
		sa.Column('amount', sa.Integer),
		sa.Column('department', sa.String(255)),
		sa.Column('created_at', sa.DateTime()),
        sa.Column('updated_at', sa.DateTime()),
        # sa.Column('created_at', sa.DateTime(), server_default=sa.func.current_timestamp()),
        # sa.Column('updated_at', sa.DateTime(), server_default=sa.func.current_timestamp()),
	)


def downgrade():
    op.drop_table('users')
