"""create transactions table

Revision ID: 09913b2d713b
Revises: 8b7a8122c3e3
Create Date: 2019-04-13 21:35:17.126500

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '09913b2d713b'
down_revision = '8b7a8122c3e3'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
		'transactions',
		sa.Column('id', sa.String(36), primary_key=True),
		sa.Column('destination', sa.String(255)),
		sa.Column('currency', sa.String(255)),
		sa.Column('card_id', sa.String(255)),
		sa.Column('amount', sa.Integer),
		sa.Column('created_at', sa.DateTime()),
        sa.Column('updated_at', sa.DateTime()),
	)


def downgrade():
    op.drop_table('transactions')
