"""add Bearers table

Revision ID: 8b7a8122c3e3
Revises: b7f9d25fecff
Create Date: 2019-04-13 13:46:36.041271

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8b7a8122c3e3'
down_revision = 'b7f9d25fecff'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
		'bearers',
		sa.Column('id', sa.String(36), primary_key=True),
		sa.Column('access_token', sa.String(255)),
		sa.Column('refresh_token', sa.String(255)),
		sa.Column('scope', sa.Text),
		sa.Column('created_at', sa.DateTime()),
        sa.Column('updated_at', sa.DateTime()),
	)


def downgrade():
    op.drop_table('bearers')
