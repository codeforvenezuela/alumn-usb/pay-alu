from dashboard import app, db, uphold_url
from dashboard.models.bearer import Bearer
import requests
import uuid
import json

class Transaction(db.Model):
	__tablename__ = "transactions"

	id = db.Column(db.String(36), primary_key=True, unique=True)
	currency = db.Column(db.String(255))
	amount = db.Column(db.Integer)
	destination = db.Column(db.String(255))
	card_id = db.Column(db.String(255))


	def __init__(self, currency, amount, destination, card_id):
		self.id = str(uuid.uuid4())
		self.currency = currency
		self.amount	= amount
		self.destination = destination
		self.card_id = card_id


	def save(self):		
		bearer = Bearer.query.order_by("created_at desc").first()
		endpoint = uphold_url + "/v0/me/cards/{}/transactions?commit=true".format(self.card_id)
		headers = {
			"Content-Type": "application/json",
			"Authorization": "Bearer {}".format(bearer.access_token),
		}

		data = {
			'denomination': { 
				'amount': float(self.amount/100),
				'currency': self.currency
			},
			'destination': self.destination
		}
		try:
			response = requests.post(endpoint, headers=headers, data=json.dumps(data))

			db.session.add(self)
			db.session.commit()
		except Exception as error:
			print("Error: {}".format(error))
			return None

		return response.json()

	def serialize(self):
		return {
			"id": self.id,
			"currency": self.currency,
			"amount": self.amount,
			"destination": self.destination,
			"card_id": self.card_id,
		}