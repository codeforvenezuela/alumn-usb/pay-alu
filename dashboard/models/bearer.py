from dashboard import db, app, admin
from flask_admin.contrib.sqla import ModelView
import uuid


class Bearer(db.Model):
	__tablename__ = "bearers"

	id = db.Column(db.Integer, primary_key=True)
	access_token = db.Column(db.String(255))
	refresh_token = db.Column(db.String(255))
	scope = db.Column(db.Text())

	def __init__(self, access_token, refresh_token, scope):
		self.id = str(uuid.uuid4())
		self.access_token = access_token
		self.refresh_token = refresh_token
		self.scope = scope

	def save(self):
		db.session.add(self)
		db.session.commit()

admin.add_view(ModelView(Bearer, db.session))