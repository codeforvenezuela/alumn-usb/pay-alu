from dashboard.models.bearer import Bearer
import requests
from dashboard import uphold_url


class Card():
	@classmethod
	def get_cards(cls):
		bearer = Bearer.query.order_by("created_at desc").first()
		endpoint = uphold_url + "/v0/me/cards"
		headers = {'Authorization': "Bearer {}".format(bearer.access_token)}
		response = requests.get(endpoint, headers=headers)
		return response.json()

	@classmethod
	def get_card(cls, card_id):
		bearer = Bearer.query.order_by("created_at desc").first()
		endpoint = uphold_url + "/v0/me/cards/{}".format(card_id)
		headers = {'Authorization': "Bearer {}".format(bearer.access_token)}
		card = requests.get(endpoint, headers=headers)
		return card.json()