from dashboard import db, app, admin
from flask_admin.contrib.sqla import ModelView
import uuid

class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.String(36), primary_key=True)
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    uphold_id = db.Column(db.String(255), unique=True)
    amount = db.Column(db.Integer)
    department = db.Column(db.String(255))

    # def __init__(self):
    #     self.id = str(uuid.uuid4())
    #     # self.first_name = first_name
    #     # self.last_name = last_name
    #     # self.email = email
    #     # self.uphold_id = uphold_id
    #     # # Amounts are stored in cents
    #     # self.amount = amount * 100
    #     # self.department = department

    def __init__(self,
                first_name=None,
                last_name=None,
                email=None,
                uphold_id=None,
                amount=0,
                department=None):
        self.id = str(uuid.uuid4())
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.uphold_id = uphold_id
        # Amounts are stored in cents
        self.amount = amount
        self.department = department

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def serialize(self):
        return {"id": self.id,
                "first_name": self.first_name,
                "last_name": self.last_name,
                "email": self.email,
                "uphold_id": self.uphold_id,
                "amount": self.amount,
                "department": self.department,
                }

admin.add_view(ModelView(User, db.session))

