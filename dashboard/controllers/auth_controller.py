from dashboard import app, db, uphold_url, auth
from flask import Blueprint, request, jsonify, Response
import json
import requests
from dashboard.models.bearer import Bearer
from flask import redirect
import os

blueprint = Blueprint("auth", __name__)

CLIENT_ID = os.environ['CLIENT_ID']
CLIENT_SECRET = os.environ['CLIENT_SECRET']

# 4a68ace5f5ebd1d9b363ae37c1b522987c19740f:007078c0f1124e24934869b3af7c5a32c0068a10

@app.route("/api/auth", methods=["GET"])
@auth.login_required
def auth_redirect():
    code = request.args.get('code')

    endpoint = uphold_url + '/oauth2/token'
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    data = {
        'code': code,
        'grant_type': 'authorization_code'
    }

    response = requests.post(endpoint, headers=headers, data=data, auth=(CLIENT_ID, CLIENT_SECRET))
    json_object = response.json()

    bearer = Bearer(access_token=json_object['access_token'],
            refresh_token=json_object['refresh_token'],
            scope=json_object['scope'],
            )
    bearer.save()

    return redirect("/users", code=302)
