import os.path

from flask import send_file, redirect
from dashboard import app, auth

build_dir = os.path.normpath(os.path.join(os.getcwd(), 'client/build'))

@app.route("/favicon.png", methods=["GET"])
def favicon():
    return send_file(os.path.join(build_dir, "favicon.png"))

@app.route("/manifest.json", methods=["GET"])
def manifest():
    return send_file(os.path.join(build_dir, "manifest.json"))

@app.route("/", methods=["GET"])
@auth.login_required
def index():
    return redirect("/users", code=302)

@app.route("/users", methods=["GET"])
@auth.login_required
def users_client():
    return send_file(os.path.join(build_dir, "index.html"))
