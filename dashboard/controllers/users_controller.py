from dashboard import app, auth
from flask import Blueprint, request, jsonify, Response
from dashboard.models.user import User
from dashboard.models.card import Card
from dashboard.models.transaction import Transaction
import json
from sqlalchemy.exc import IntegrityError


blueprint = Blueprint("users", __name__)


@app.route("/api/users", methods=["POST"])
@auth.login_required
def user_create():
    payload = request.json
    user = User(
        first_name=payload['first_name'],
        last_name=payload['last_name'],
        email=payload['email'],
        uphold_id=payload['uphold_id'],
        amount=payload['amount'],
        department=payload['department'],
    )

    try:
        user.save()
    except IntegrityError as error:
        return "User exist (email or uphold_id). {}".format(error), 500
    except Exception as error:
        return error, 500


    return jsonify(user.serialize()), 200

@app.route("/api/users", methods=["GET"])
@auth.login_required
def users():
    results = User.query.all()
    return jsonify([user.serialize() for user in results])
    
@app.route("/api/users/cards", methods=["GET"])
@auth.login_required
def user_cards():
    cards = Card.get_cards()
    return jsonify(cards), 200
    

@app.route("/api/users/pay", methods=["POST"])
@auth.login_required
def pay():
    payload = request.json
    card_id = payload['card_id']
    card = Card.get_card(card_id)
    transaction = Transaction(
                    currency = card['currency'],
                    amount=payload['amount'],
                    destination=payload['destination'],
                    card_id=card_id,
                    )
    response = transaction.save()
    return jsonify(response), 200
    

@app.route("/api/users/<uphold_id>/transactions", methods=["GET"])
@auth.login_required
def user_transaction(uphold_id):
    transactions = Transaction.query.filter_by(destination=uphold_id).all()
    return jsonify([transaction.serialize() for transaction in transactions]), 200

@app.route("/api/users/<id>/pay", methods=["POST"])
@auth.login_required
def user_pay(id):
    cards = Card.get_cards()
    return "card", 200

@app.route("/api/users/<id>", methods=["PUT"])
@auth.login_required
def user_update(id):
    user = User.query.filter_by(id=id).first()
    payload = request.json

    user.first_name=payload['first_name']
    user.last_name=payload['last_name']
    user.uphold_id=payload['uphold_id']
    user.amount=payload['amount']
    user.department=payload['department']
    user.save()

    return jsonify(user.serialize()), 200


@app.route("/api/users/<id>", methods=["DELETE"])
@auth.login_required
def user_delete(id):
    user = User.query.filter_by(id=id).first()    
    user.delete()

    return '', 204
