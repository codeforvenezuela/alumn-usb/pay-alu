from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_sslify import SSLify
from dashboard.lib.uphold import Uphold
from flask_httpauth import HTTPBasicAuth
import os



auth = HTTPBasicAuth()
static_folder = os.path.normpath(os.path.join(os.getcwd(), 'client/build/static'))

app = Flask(__name__, static_folder=static_folder)
sslify = SSLify(app)
db = SQLAlchemy(app)
admin = Admin(app, name='Uphold Dashboard', template_mode='bootstrap3')
uphold_url = os.environ['UPHOLD_URL']


from dashboard.controllers import (
    client_controller,
    auth_controller,
    users_controller
)

# from dashboard.models.user import User
app.config['SECRET_KEY'] = '651cbc5a-e74b-4843-9f40-dde4cdc3e40c'
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = os.environ["SQLALCHEMY_DATABASE_URI"]


from dashboard.models.user import User

users = {
    os.environ['ADMIN_USER']: os.environ['ADMIN_PASSWORD']
}

@auth.get_password
def get_pw(username):
    if username in users:
        return users.get(username)
    return None

