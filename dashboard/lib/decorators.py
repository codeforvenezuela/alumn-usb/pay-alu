from flask import request
from functools import wraps

def origin_localhost(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if request.host != '0.0.0.0:5000':
            return "denied for " + request.host
        return func(*args, **kwargs)

    return decorated_function