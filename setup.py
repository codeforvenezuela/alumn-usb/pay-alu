from setuptools import setup

setup(
    name='py-uphold-dashboard',
    packages=['py-uphold-dashboard'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)