#!/usr/bin/env python

import os
from dashboard import app as application

if os.environ["APP_DEBUG_MODE"] == "False":
    debug_mode = False
else:
    debug_mode = True


application.run(debug=debug_mode, host='0.0.0.0', port=os.environ["PORT"])

