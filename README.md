https://github.com/code-for-venezuela/2019-april-codeathon
https://devpost.com/software/automating-scholarship-payments

## Requirements
 - Docker

## Usage
This app uses docker container to run the app in an insolated environemet. To start the container run:
```
docker-compose up
```
Go to http://localhost:5000

## Development with no contianers

Before you work on your project, activate the corresponding environment:
```
$ . venv/bin/activate
```

### How to run https on local
```
brew install node
npm install -g ngrok
ngrok http 5000
```

### How to run the client for dev
```
cd client/
npm install
npm start
```
