FROM mysql:5.7

LABEL maintainer="antoine.falquerho@gmail.com"
LABEL maintainer="mcampa@gmail.com"

RUN apt-get update
RUN apt-get install -y python3-pip python3-dev libmysqlclient-dev build-essential libssl-dev libffi-dev libxml2-dev libxslt1-dev zlib1g-dev netcat
RUN ln -s /usr/bin/python3 /usr/local/bin/python
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools wheel

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /src/requirements.txt

WORKDIR /src

RUN pip3 install -r requirements.txt
COPY . /src

# Expose ports
EXPOSE 5000

# Run the app
CMD chmod +x ./entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
