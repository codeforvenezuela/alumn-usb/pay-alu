#!/bin/bash

echo "starting entrypoint.sh"

echo "running any pending migration"
alembic upgrade head

# Run the app
echo "starting the app"
python3 app.py
