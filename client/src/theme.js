import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
  palette: {
    // type: 'dark',
    primary: { main: '#d4ba35' },
    secondary: { main: '#3d5afe' },
  },
  mixins: {
    toolbar: {
      minHeight: 55,
    },
  },
  typography: {
    useNextVariants: true,
  },
});
