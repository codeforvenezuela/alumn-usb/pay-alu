import { remote, remoteGet } from 'api/remote';
import { camelizeKeys } from 'humps';

export async function getUsers() {
  const response = await remoteGet('/api/users');
  return response.data.map(user => camelizeKeys(user));
}

export async function getUserCards() {
  const response = await remoteGet('/api/users/cards');
  return response.data;
}

export async function getUserTransactions(user) {
  const response = await remoteGet(`/api/users/${user.email}/transactions`);
  return response.data.map(user => camelizeKeys(user));
}

export async function payUser({ amount, email, cardId }) {
  const response = await remote.post('/api/users/pay', {
    amount: amount,
    destination: email,
    card_id: cardId,
  });

  if (response.data && response.data.code === 'validation_failed') {
    throw new Error('Validation failed, please check you selected the correct pay source');
  }

  return response.data;
}

export async function updateUser(user) {
  const response = await remote.put(`/api/users/${user.id}`, {
    amount: user.amount,
    department: user.department,
    email: user.email,
    first_name: user.firstName,
    last_name: user.lastName,
    uphold_id: user.email,
  });

  if (response.data && response.data.code === 'validation_failed') {
    throw new Error('Validation failed, please check you selected the correct pay source');
  }

  return response.data;
}

export const createUser = user => {
  return remote.post('/api/users', {
    amount: user.amount,
    department: user.department,
    email: user.email,
    first_name: user.firstName,
    last_name: user.lastName,
    uphold_id: user.email,
  });
};
