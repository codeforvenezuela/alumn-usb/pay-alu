import axios from 'axios';
import queryString from 'query-string';

export const remote = axios.create({
  // xsrfCookieName: "XSRF-TOKEN",
  headers: { 'X-Requested-With': 'XMLHttpRequest' },
});

export const remoteGet = (path, params = null) => {
  const url = path + (params ? `?${queryString.stringify(params)}` : '');
  return remote.get(url);
};
