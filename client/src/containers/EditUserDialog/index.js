import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CurrencyField from 'containers/CurrencyField';
import FormGroup from '@material-ui/core/FormGroup';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  formControl: {
    margin: '20px 0',
  },
});

function EditUserDialog(props) {
  const { classes, children, user, onChange, title } = props;
  const [open, setOpen] = React.useState(false);

  const [firstName, setFirstName] = React.useState(user.firstName);
  const [lastName, setLastName] = React.useState(user.lastName);
  const [email, setEmail] = React.useState(user.email);
  const [amount, setAmount] = React.useState(user.amount);
  const [department, setDepartment] = React.useState(user.department);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleFirstNameChange = e => setFirstName(e.target.value);
  const handleLastNameChange = e => setLastName(e.target.value);
  const handleEmailChange = e => setEmail(e.target.value);
  const handleAmountChange = amount => setAmount(amount);
  const handleDepartmentChange = e => setDepartment(e.target.value);

  const handleSaveUser = async () => {
    await onChange({
      ...user,
      firstName,
      lastName,
      email,
      amount,
      department,
    });

    handleClose();
  };

  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        {children}
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" fullWidth maxWidth="sm">
        <DialogTitle id="form-dialog-title">{title}</DialogTitle>
        <DialogContent>
          <FormGroup>
            <FormControl className={classes.formControl}>
              <FormLabel component="legend">First Name</FormLabel>
              <TextField value={firstName} onChange={handleFirstNameChange} autoFocus />
            </FormControl>
            <FormControl className={classes.formControl}>
              <FormLabel component="legend">Last Name</FormLabel>
              <TextField value={lastName} onChange={handleLastNameChange} />
            </FormControl>
            <FormControl className={classes.formControl}>
              <FormLabel component="legend">Email</FormLabel>
              <TextField value={email} onChange={handleEmailChange} />
            </FormControl>
            <FormControl className={classes.formControl}>
              <FormLabel component="legend">Amount to pay</FormLabel>
              <CurrencyField defaultValue={amount} onChange={handleAmountChange} />
            </FormControl>
            <FormControl className={classes.formControl}>
              <FormLabel component="legend">Type</FormLabel>
              <RadioGroup aria-label="Type" name="department" value={department} onChange={handleDepartmentChange}>
                <FormControlLabel value="student" control={<Radio />} label="Student" />
                <FormControlLabel value="teacher" control={<Radio />} label="Teacher" />
              </RadioGroup>
            </FormControl>
          </FormGroup>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="default">
            Cancel
          </Button>
          <Button onClick={handleSaveUser} color="primary" variant="outlined">
            Save User
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default withStyles(styles)(EditUserDialog);
