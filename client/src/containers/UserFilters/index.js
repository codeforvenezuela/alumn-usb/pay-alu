import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  root: {
    flexGrow: 1,
  },
};

function UserFilters(props) {
  const { classes, onChange } = props;
  const [type, setType] = React.useState('all');

  React.useEffect(() => {
    onChange({
      type,
    });
  }, [type]);

  function handleTypeChange(e) {
    setType(e.target.value);
  }

  return (
    <>
      <FormControl className={classes.formControl}>
        <Select
          value={type}
          onChange={handleTypeChange}
          inputProps={{
            name: 'type',
            id: 'filter-type',
          }}
        >
          <MenuItem value={'all'}>All</MenuItem>
          <MenuItem value={'teacher'}>Teachers</MenuItem>
          <MenuItem value={'student'}>Students</MenuItem>
        </Select>
      </FormControl>
    </>
  );
}

export default withStyles(styles)(UserFilters);
