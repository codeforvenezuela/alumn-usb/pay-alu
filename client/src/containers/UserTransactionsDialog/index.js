import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import { withStyles } from '@material-ui/core/styles';
import { getUserTransactions } from 'api/users';

function money(cents) {
  return (Number(cents) / 100).toLocaleString('en-US', {
    style: 'currency',
    currency: 'USD',
  });
}

const styles = theme => ({
  formControl: {
    margin: '20px 0',
  },
});

function TransactionTable(props) {
  const { onClose, user } = props;
  const [transactions, setTransactions] = React.useState([]);

  React.useEffect(() => {
    loadTransactions();
  }, []);

  async function loadTransactions() {
    const transactions = await getUserTransactions(user);
    setTransactions(transactions);
  }

  return (
    <div>
      <Dialog open onClose={onClose} aria-labelledby="form-dialog-title" fullWidth maxWidth="lg">
        <DialogTitle id="form-dialog-title">Transactions</DialogTitle>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Destination</TableCell>
              <TableCell align="left">Card ID</TableCell>
              <TableCell align="left">Transaction ID</TableCell>
              <TableCell align="right">Amount</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {(transactions || []).map(transaction => (
              <TableRow key={transaction.id}>
                <TableCell align="left">{transaction.cardId}</TableCell>
                <TableCell align="left">{transaction.destination}</TableCell>
                <TableCell align="left">{transaction.id}</TableCell>
                <TableCell align="right">
                  {money(transaction.amount)} {transaction.currency}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <DialogActions>
          <Button onClick={onClose} color="default">
            CLose
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

function UserTransactionsDialog(props) {
  const { user } = props;
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        History
      </Button>
      {open && (
        <div>
          <TransactionTable user={user} onClose={handleClose} />
        </div>
      )}
    </div>
  );
}

export default withStyles(styles)(UserTransactionsDialog);
