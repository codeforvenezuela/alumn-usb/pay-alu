import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import React from 'react';

const useStyles = makeStyles({
  paper: {
    padding: '0 15px',
    width: '1140px',
  },
});

PageContent.defaultProps = {
  alignItems: 'center',
  direction: 'row',
  justify: 'center',
};

export default function PageContent(props) {
  const { alignItems, children, direction, justify } = props;
  const classes = useStyles();

  return (
    <Grid container>
      <Grid item xs={12}>
        <Grid container alignItems={alignItems} direction={direction} justify={justify}>
          <div className={classes.paper}>{children}</div>
        </Grid>
      </Grid>
    </Grid>
  );
}
