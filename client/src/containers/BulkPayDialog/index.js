import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import green from '@material-ui/core/colors/green';
import CheckIcon from '@material-ui/icons/Check';
import ErrorIcon from '@material-ui/icons/Error';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import { getUserCards, payUser } from 'api/users';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

function money(cents) {
  return (Number(cents) / 100).toLocaleString('en-US', {
    style: 'currency',
    currency: 'USD',
  });
}
const styles = theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  row: {
    display: 'flex',
    margin: '10px 0',
  },
  wrapper: {
    margin: theme.spacing.unit,
    position: 'relative',
  },
  buttonSuccess: {
    backgroundColor: green[500],
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    top: -6,
    left: -6,
    zIndex: 1,
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
});

// let payCounter = 0;
// function payUser(email) {
//   return new Promise(resolve => {
//     console.log('paying', email);
//     setTimeout(() => {
//       console.log('done', email);
//       resolve();
//     }, 600 + payCounter * 400);
//     console.log(600 + payCounter * 400);
//     payCounter++;
//   });
// }

function BulkPayDialog(props) {
  const { classes, users, onClose } = props;
  const [cards, setCards] = React.useState(false);
  const [selectedCard, setSelectedCard] = React.useState(null);
  const [loadingCards, setLoadingCards] = React.useState(false);
  const [paying, setPaying] = React.useState(false);
  const [successPayedUsers, setSuccessPayedUsers] = React.useState(new Set());
  const [failPayedUsers, setFailPayedUsers] = React.useState(new Set());

  React.useEffect(() => {
    loadCards();
  }, []);

  async function loadCards() {
    setLoadingCards(true);
    try {
      const cards = await getUserCards();
      setCards(cards);
      if (cards.length > 0 && !selectedCard) {
        setSelectedCard(cards[0].id);
      }
    } catch (error) {
      // TODO
    }
    setLoadingCards(false);
  }

  async function handleBulkPay() {
    setPaying(true);
    const promises = [...users]
      // filter already successful payments
      .filter(user => !successPayedUsers.has(user))
      .map(async user => {
        successPayedUsers.has(user) && successPayedUsers.delete(user);
        failPayedUsers.has(user) && failPayedUsers.delete(user);
        try {
          await payUser({
            amount: user.amount,
            email: user.email,
            cardId: selectedCard,
          });
          successPayedUsers.add(user);
          setSuccessPayedUsers(new Set(successPayedUsers));
        } catch (e) {
          console.error(e);
          failPayedUsers.add(user);
          setFailPayedUsers(new Set(failPayedUsers));
        }
      });

    await Promise.all(promises);
    await loadCards();
    setPaying(false);
  }

  function handleCardChange(e) {
    setSelectedCard(e.target.value);
  }

  const allDone = !paying && users.size === successPayedUsers.size && failPayedUsers.size === 0;

  return (
    <Dialog open onClose={onClose} aria-labelledby="form-dialog-title" fullWidth maxWidth="lg">
      <DialogTitle id="form-dialog-title">Process bulk payments</DialogTitle>
      <DialogContent>
        <div className={classes.row}>
          <FormGroup row>
            <FormControlLabel
              labelPlacement="start"
              control={
                <Select
                  value={selectedCard}
                  onChange={handleCardChange}
                  inputProps={{
                    name: 'age',
                    id: 'age-simple',
                  }}
                >
                  {cards &&
                    cards.map(card => (
                      <MenuItem key={card.id} value={card.id}>
                        {card.label} ({card.balance} {card.currency})
                      </MenuItem>
                    ))}
                </Select>
              }
              label={<>Payment source {loadingCards && <CircularProgress />}</>}
            />
          </FormGroup>
        </div>
        <Table>
          <TableBody>
            {[...users].map(user => (
              <TableRow key={user.id}>
                <TableCell align="left">
                  <div>
                    {user.firstName} {user.lastName}
                  </div>
                  <div>{user.email}</div>
                </TableCell>
                <TableCell align="left" />
                <TableCell align="left">{user.type}</TableCell>
                <TableCell align="left">{money(user.amount)}</TableCell>
                <TableCell align="right">
                  <div className={classes.wrapper}>
                    {successPayedUsers.has(user) && <CheckIcon style={{ color: 'green' }} />}
                    {failPayedUsers.has(user) && <ErrorIcon color="error" />}
                    {paying && !(successPayedUsers.has(user) || failPayedUsers.has(user)) && (
                      <>
                        <MonetizationOnIcon />
                        <CircularProgress size={36} className={classes.fabProgress} />
                      </>
                    )}
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Close</Button>
        {allDone ? (
          <Button onClick={onClose} color="primary" variant="outlined">
            Done
          </Button>
        ) : (
          <Button disabled={paying || !selectedCard} onClick={handleBulkPay} color="primary" variant="outlined">
            {failPayedUsers.size > 0 && !paying ? 'Retry' : 'Process Payments'}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
}

const BulkPayDialogWithStyles = withStyles(styles)(BulkPayDialog);

export default function BulkPayButton(props) {
  const [open, setOpen] = React.useState(false);

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }
  return (
    <div className={props.className}>
      <Button variant="outlined" color="default" onClick={handleClickOpen}>
        {props.children}
      </Button>
      {open && <BulkPayDialogWithStyles users={props.users} onClose={handleClose} />}
    </div>
  );
}
