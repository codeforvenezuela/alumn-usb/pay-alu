import React from 'react';
import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
  formControl: {
    margin: theme.spacing.unit,
  },
});

function CurrencyInput(props) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            value: values.value,
          },
        });
      }}
      thousandSeparator
      prefix="$"
      style={{ textAlign: 'right' }}
    />
  );
}

CurrencyInput.propTypes = {
  inputRef: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

function CurrencyField(props) {
  const { classes, defaultValue, onChange, onEnter, ...moreProps } = props;
  const [value, setValue] = React.useState(defaultValue / 100);
  const [loading, setLoading] = React.useState(false);

  const handleChange = event => {
    setValue(event.target.value);
    onChange && onChange(event.target.value);
  };

  const handleKeyPress = async event => {
    if (event.key === 'Enter') {
      setLoading(true);
      const amount = parseFloat(event.target.value.replace(/\$|,/g, '')) * 100;
      try {
        onEnter && onEnter(amount);
      } catch (error) {}
      setLoading(false);
    }
  };

  return (
    <>
      <TextField
        InputProps={{
          inputComponent: CurrencyInput,
        }}
        {...moreProps}
        value={value}
        onChange={handleChange}
        onKeyPress={handleKeyPress}
      />
      {loading && <CircularProgress />}
    </>
  );
}

export default withStyles(styles)(CurrencyField);
