import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import PageContent from '../PageContent';
import BulkPayDialog from '../BulkPayDialog';
import UserFilters from '../UserFilters';
import EditUserDialog from '../EditUserDialog';
import UserTransactionsDialog from '../UserTransactionsDialog';
import { getUsers, updateUser, createUser, getUserCards } from 'api/users';
import CurrencyField from 'containers/CurrencyField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Checkbox from '@material-ui/core/Checkbox';

function money(cents) {
  return Number(cents).toLocaleString('en-US', {
    style: 'currency',
    currency: 'USD',
  });
}

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  toolBar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
});

const defaultUser = {
  amount: 5000,
  department: 'student',
  email: '',
  firstName: '',
  lastName: '',
  upholdId: '',
};

function UsersPage(props) {
  const { classes } = props;
  const [balance, setBalance] = React.useState(null);
  const [users, setUsers] = React.useState(null);
  const [filters, setFilters] = React.useState({ type: 'all' });
  const [selectedUsers, setSelectedUsers] = React.useState(new Set());

  React.useEffect(() => {
    setSelectedUsers(new Set());
  }, [users, filters]);

  React.useEffect(() => {
    loadUsers();
  }, []);

  React.useEffect(() => {
    loadBalance();
  }, [users]);

  async function loadUsers() {
    const users = await getUsers();
    setUsers(users);
  }
  async function loadBalance() {
    try {
      const cards = await getUserCards();
      let balance = 0;
      cards.forEach(card => {
        console.log(card);
        balance += parseFloat(card.balance);
      });
      setBalance(balance);
    } catch (error) {
      // TODO
    }
  }

  function handleFilterChange(newFilters) {
    setFilters(newFilters);
  }

  function handleSelectAllAccounts() {
    if (selectedUsers.size > 0) {
      setSelectedUsers(new Set());
    } else {
      setSelectedUsers(new Set(users));
    }
  }

  function handleSelectUser(user) {
    return () => {
      const newSelectedUsers = new Set(selectedUsers);

      if (newSelectedUsers.has(user)) {
        newSelectedUsers.delete(user);
      } else {
        newSelectedUsers.add(user);
      }

      setSelectedUsers(newSelectedUsers);
    };
  }

  function handleUserAmountChange(user) {
    return async amount => {
      const newUser = { ...user, amount };
      const index = users.findIndex(u => u.id === user.id);
      if (index >= 0) {
        users[index] = newUser;
      }
      setUsers([...users]);

      await updateUser(newUser);
      await loadUsers();
    };
  }

  async function handleUserChange(user) {
    await updateUser(user);
    await loadUsers();
  }

  async function handleCreateUser(user) {
    await createUser(user);
    await loadUsers();
  }

  const allSelected = users && users.length === selectedUsers.size;
  // const someSelected = selectedUsers.size > 0 && !allSelected;

  return (
    <PageContent>
      {balance !== null && (
        <AppBar position="static" color="secondary" style={{ marginTop: '10px' }}>
          <Toolbar>You have ${money(balance)} USD available</Toolbar>
        </AppBar>
      )}
      <Paper className={classes.root}>
        <AppBar position="static">
          <Toolbar className={classes.toolBar}>
            <div>
              <UserFilters onChange={handleFilterChange} />
            </div>
            <div>
              {selectedUsers.size > 0 ? (
                <BulkPayDialog users={selectedUsers}>Bulk pay</BulkPayDialog>
              ) : (
                <EditUserDialog user={{ ...defaultUser }} onChange={handleCreateUser} title="Add new user">
                  Add new user
                </EditUserDialog>
              )}
            </div>
          </Toolbar>
        </AppBar>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell padding="checkbox">
                <Checkbox
                  indeterminate={selectedUsers.size > 0}
                  checked={allSelected}
                  onChange={handleSelectAllAccounts}
                />
              </TableCell>
              <TableCell>Name/Email</TableCell>
              <TableCell align="left">Teacher/Student</TableCell>
              <TableCell align="left">Amount</TableCell>
              <TableCell align="right">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users &&
              users
                .filter(user => filters.type === 'all' || user.department === filters.type)
                .map(user => (
                  <TableRow key={user.id}>
                    <TableCell padding="checkbox">
                      <Checkbox checked={selectedUsers.has(user)} onChange={handleSelectUser(user)} />
                    </TableCell>
                    <TableCell align="left">
                      <div>
                        {user.firstName} {user.lastName}
                      </div>
                      <div>{user.email}</div>
                    </TableCell>
                    <TableCell align="left">{user.department}</TableCell>
                    <TableCell align="left">
                      <CurrencyField defaultValue={user.amount} onEnter={handleUserAmountChange(user)} />
                    </TableCell>
                    <TableCell align="right">
                      <div className={classes.toolBar}>
                        <EditUserDialog user={user} onChange={handleUserChange} title="Edit user">
                          Edit
                        </EditUserDialog>
                        <UserTransactionsDialog user={user} />
                        <BulkPayDialog users={new Set([user])}>Pay</BulkPayDialog>
                        {/* <UserMenu /> */}
                      </div>
                    </TableCell>
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </Paper>
    </PageContent>
  );
}

UsersPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UsersPage);
