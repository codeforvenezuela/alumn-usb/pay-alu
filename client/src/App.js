import './bootstrap';

import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import ThemeProvider from '@material-ui/styles/ThemeProvider';
import theme from './theme';
import UsersPage from './containers/UsersPage';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import NoMatch from 'containers/NoMatch';
import logo from './logo.svg';
import 'App.css';
import SignIn from 'containers/SignIn';

function AppHeader() {
  return (
    <AppBar position="static" color="primary">
      <Toolbar className="App-Toolbar">
        <img src={logo} className="App-logo" alt="logo" />
        <Typography variant="h6" color="inherit">
          AlumnUSB Payment System
        </Typography>
      </Toolbar>
    </AppBar>
  );
}

export default function App() {
  return (
    <Router>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <AppHeader />
        <Switch>
          <Route path="/" exact component={SignIn} />
          <Route path="/users" exact component={UsersPage} />
          <Route component={NoMatch} />
        </Switch>
      </ThemeProvider>
    </Router>
  );
}
